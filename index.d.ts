import {Action, AnyAction, Reducer} from 'redux';

export type SectionReducer<S = any, A extends Action = AnyAction, SE = any> = (state: S | undefined, action: A, entireState: SE) => S;

export type SectionReducersMapObject<S = any, A extends Action = Action> = {
    [K in keyof S]: SectionReducer<S[K], A, S>;
};

export default function combineSectionReducers<S, A extends Action = AnyAction, SE = any>(reducers: SectionReducersMapObject<S, A, SE>): Reducer<S, A>;
export default function combineSectionReducers<S, A extends Action = AnyAction>(reducers: SectionReducersMapObject<S, A>): Reducer<S, A>;
export default function combineSectionReducers<S>(reducers: SectionReducersMapObject<S, any>): Reducer<S>;
